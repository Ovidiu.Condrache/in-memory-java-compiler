# In Memory Java Compiler
## Motivation
This implementation of an In-Memory Java Compiler tries to overcome certain limitations introduced by some of the implementations mentioned as references below. 
<br><br>
For instance, the implementation mentioned as Reference [1], fails to compile files that contain various inner classes. This happens because there isn't a one to one
relationship between a *.java* file and a *.class* file and in the case of inner classes, additional class files are generated, which accounts for the necessity of
multiple In-Memory Java Class Objects.<br><br>
As for the implementation mentioned as Reference [2], there is no possibility to compile a file with dependencies, without compiling the actual dependency first.
Other than that, the compilation process is handled quite well.<br><br>
The implementation mentioned as Reference [3] is the one which introduces an implementation of the *list()* method declared by the *StandardJavaFileManager*.
This specific method is very important for the whole process, as it tells the compiler the files that it needs in order to perform the actual task.<br><br>
In the end, the implementation mentioned as Reference [4] introduces the programmatic usage of the compilation options, such as *sourcepath* or *classpath*.<br><br>
## Basic Usage
<pre><code>String sourceCode = ...; 
InMemoryJavaFileObject inMemoryJavaFileObject = new InMemoryJavaFileObject("fully.qualified.class.name", sourceCode);
InMemoryJavaCompiler.compile(inMemoryJavaFileObject, false);
</code></pre><br>
The resulting bytecode will be available as part of the *InMemoryJavaFileSystem* :
<pre><code>byte[] inMemoryBytecode = InMemoryJavaFileSystem.getInstance().getInMemoryJavaClass("fully.qualified.class.name").getBytecode();
</code></pre><br>
The second parameter, ***compileWithSourcePath***, is used to compile files based on *sourcepath* option. Instead of resolving dependencies based on the existence of the necessary
*.class* files, the compiler uses the necessary *.java* files instead, by checking the *InMemoryJavaFileSystem*. Given a file called File1, which depends on a file called File 2,
the following snippet shows how File1 can be compiled, without compiling File2 first or compiling File1 and File2 at the same time :
<pre><code>String sourceCodeFile1 = ...;
String sourceCodeFile2 = ...;
InMemoryJavaFileObject inMemoryJavaFile1 = new InMemoryJavaFileObject("fully.qualified.class.name.file1", sourceCodeFile1);
InMemoryJavaFileObject inMemoryJavaFile2 = new InMemoryJavaFileObject("fully.qualified.class.name.file2", sourceCodeFile2);

InMemoryJavaFileSystem.getInstance().putInMemoryJavaFile(inMemoryJavaFile2);
InMemoryJavaCompiler.compile(inMemoryJavaFile1, true);
</pre></code><br>
For other usages, please consult the content of the following JUnit tests file : https://gitlab.com/Ovidiu.Condrache/in-memory-java-compiler/blob/master/in-memory-java-compiler/src/test/java/com/gitlab/oc/inmemoryjavacompiler/core/InMemoryJavaCompilerTests.java
<br><br>
## Maven Dependency
<code>\<dependency\>
    \<groupId\>com.gitlab.ovidiucondrache\</groupId\>
    \<artifactId\>in-memory-java-compiler\</artifactId\>
    \<version\>1.0.0\</version\>
\</dependency\></code>
<br><br>
Artifact is pushed to OSS Sonatype Releases Repository : https://oss.sonatype.org/content/repositories/releases/
<br><br>
## REFERENCES 
[1] https://github.com/trung/InMemoryJavaCompiler <br>
[2] https://github.com/OpenHFT/Java-Runtime-Compiler <br>
[3] http://pfmiles.github.io/blog/dynamic-java/ <br>
[4] http://www.soulmachine.me/blog/2015/07/22/compile-and-run-java-source-code-in-memory/ <br>