package com.gitlab.oc.inmemoryjavacompiler.core;

import java.util.Arrays;
import java.util.List;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.ToolProvider;

public final class InMemoryJavaCompiler {
	
	private static final JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();
	private static final DiagnosticListener<? super JavaFileObject> diagnosticListener = new DiagnosticListener<JavaFileObject>() {
		@Override
		public void report(Diagnostic<? extends JavaFileObject> diagnostic) {
			if (diagnostic.getKind() == Diagnostic.Kind.ERROR) {
				System.err.println(diagnostic);
			}
		}
	};

	public static boolean compile(InMemoryJavaFileObject inMemoryJavaFileObject, boolean compileWithSourcePath) {
		return compileInternal(Arrays.asList(inMemoryJavaFileObject), compileWithSourcePath);
	}
	
	public static boolean compile(List<InMemoryJavaFileObject> inMemoryJavaFileObjects, boolean compileWithSourcePath) {
		return compileInternal(inMemoryJavaFileObjects, compileWithSourcePath);
	}
	
	private static boolean compileInternal(Iterable<InMemoryJavaFileObject> compilationUnits, boolean compileWithSourcePath) {
		InMemoryJavaFileManager inMemoryJavaFileManager = new InMemoryJavaFileManager(javaCompiler.getStandardFileManager(null, null, null));
		JavaCompiler.CompilationTask javaCompilationTask;
		Iterable<String> compilationOptions = null;
		
		compilationUnits.forEach(compilationUnit -> {
			InMemoryJavaFileSystem.getInstance().putInMemoryJavaFile(compilationUnit);
		});
		
		if (compileWithSourcePath)
			compilationOptions = Arrays.asList("-sourcepath", ".");
		
		javaCompilationTask = javaCompiler.getTask(null, inMemoryJavaFileManager, diagnosticListener, compilationOptions, null, compilationUnits);
		
		return javaCompilationTask.call();
	}
}
