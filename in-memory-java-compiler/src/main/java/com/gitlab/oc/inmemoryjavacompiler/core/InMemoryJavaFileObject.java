package com.gitlab.oc.inmemoryjavacompiler.core;

import java.net.URI;

import javax.tools.SimpleJavaFileObject;

public final class InMemoryJavaFileObject extends SimpleJavaFileObject {
	
	private final String className;
	private final String sourceCode;

	public InMemoryJavaFileObject(String className, String sourceCode) {
		super(URI.create("string:///" + className.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
		this.className = className;
		this.sourceCode = sourceCode;
	}
	
	@Override
	public CharSequence getCharContent(boolean ignoreEncondingErrors) {
		return sourceCode;
	}
	
	public String getClassName() {
		return className;
	}
}
