package com.gitlab.oc.inmemoryjavacompiler.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import javax.tools.SimpleJavaFileObject;

public final class InMemoryJavaClassObject extends SimpleJavaFileObject {
	
	private final String className;
	private final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	
	public InMemoryJavaClassObject(String className, Kind kind) {
		super(URI.create("string:///" + className.replace('.', '/') + kind.extension), kind);
		this.className = className;
	}
	
	@Override
	public InputStream openInputStream() {
		return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
	}
	
	@Override
	public OutputStream openOutputStream() {
		return byteArrayOutputStream;
	}
	
	public String getClassName() {
		return className;
	}
	
	public byte[] getBytecode() {
		return byteArrayOutputStream.toByteArray();
	}
}
