package com.gitlab.oc.inmemoryjavacompiler.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;

public final class InMemoryJavaFileManager extends ForwardingJavaFileManager<StandardJavaFileManager> {
	
	private static final InMemoryJavaFileSystem inMemoryJavaFileSystem = InMemoryJavaFileSystem.getInstance();
	
	public InMemoryJavaFileManager(StandardJavaFileManager standardJavaFileManager) {
		super(standardJavaFileManager);
	}
	
	@Override
	public JavaFileObject getJavaFileForOutput(Location location, String className, Kind kind, FileObject sibling) throws IOException {
		if (!(Kind.CLASS.equals(kind) && StandardLocation.CLASS_OUTPUT.equals(location)))
			return super.getJavaFileForOutput(location, className, kind, sibling);
		
		if (inMemoryJavaFileSystem.containsInMemoryJavaClass(className))
			return inMemoryJavaFileSystem.getInMemoryJavaClass(className);
		else {
			InMemoryJavaClassObject inMemoryJavaClassObject = new InMemoryJavaClassObject(className, kind);
			
			inMemoryJavaFileSystem.putInMemoryJavaClass(inMemoryJavaClassObject);
			return inMemoryJavaClassObject;
		}
	}
	
	@Override
	public Iterable<JavaFileObject> list(Location location, String packageName, Set<Kind> kinds, boolean recurse) throws IOException {
		List<JavaFileObject> returnedJavaFileObjects = new ArrayList<>();
		
		if (!(StandardLocation.CLASS_PATH.equals(location) || StandardLocation.CLASS_OUTPUT.equals(location) || StandardLocation.SOURCE_PATH.equals(location))) {
			Iterable<JavaFileObject> superList = super.list(location, packageName, kinds, recurse);
			
			if (superList != null)
				superList.forEach(returnedJavaFileObjects::add);
		}
		
		if ((StandardLocation.CLASS_OUTPUT.equals(location) || StandardLocation.CLASS_PATH.equals(location)) && kinds.contains(Kind.CLASS))
			inMemoryJavaFileSystem.listInMemoryJavaClasses(returnedJavaFileObjects, packageName, recurse);
		else if (StandardLocation.SOURCE_PATH.equals(location) && kinds.contains(Kind.SOURCE))
			inMemoryJavaFileSystem.listInMemoryJavaFiles(returnedJavaFileObjects, packageName, recurse);
		
		return returnedJavaFileObjects;
	}
	
	@Override
	public String inferBinaryName(Location location, JavaFileObject javaFileObject) {
		if (javaFileObject instanceof InMemoryJavaFileObject)
			return ((InMemoryJavaFileObject) javaFileObject).getClassName();
		else if (javaFileObject instanceof InMemoryJavaClassObject)
			return ((InMemoryJavaClassObject) javaFileObject).getClassName();
		else
			return super.inferBinaryName(location, javaFileObject);
	}
}
