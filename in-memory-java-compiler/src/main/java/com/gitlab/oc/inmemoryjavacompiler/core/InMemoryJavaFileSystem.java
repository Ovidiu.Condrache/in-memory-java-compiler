package com.gitlab.oc.inmemoryjavacompiler.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.tools.JavaFileObject;

public final class InMemoryJavaFileSystem {
	
	private static InMemoryJavaFileSystem instance;
	private static Map<String, InMemoryJavaFileObject> inMemoryJavaFiles = new HashMap<>();
	private static Map<String, InMemoryJavaClassObject> inMemoryJavaClasses = new HashMap<>();
	
	public static InMemoryJavaFileSystem getInstance() {
		if (instance == null)
			instance = new InMemoryJavaFileSystem();
		
		return instance;
	}
	
	private InMemoryJavaFileSystem() {
		
	}
	
	public InMemoryJavaFileObject getInMemoryJavaFile(String className) {
		return inMemoryJavaFiles.get(className);
	}
	
	public InMemoryJavaClassObject getInMemoryJavaClass(String className) {
		return inMemoryJavaClasses.get(className);
	}
	
	public void putInMemoryJavaFile(InMemoryJavaFileObject inMemoryJavaFileObject) {
		inMemoryJavaFiles.put(inMemoryJavaFileObject.getClassName(), inMemoryJavaFileObject);
	}
	
	public void putInMemoryJavaClass(InMemoryJavaClassObject inMemoryJavaClassObject) {
		inMemoryJavaClasses.put(inMemoryJavaClassObject.getClassName(), inMemoryJavaClassObject);
	}
	
	public boolean containsInMemoryJavaFile(String className) {
		return inMemoryJavaFiles.containsKey(className);
	}
	
	public boolean containsInMemoryJavaClass(String className) {
		return inMemoryJavaClasses.containsKey(className);
	}
	
	public Map<String, InMemoryJavaFileObject> getInMemoryJavaFiles() {
		HashMap<String, InMemoryJavaFileObject> returnedInMemoryJavaFiles = new HashMap<>();
		
		returnedInMemoryJavaFiles.putAll(inMemoryJavaFiles);
		
		return returnedInMemoryJavaFiles;
	}
	
	public Map<String, InMemoryJavaClassObject> getInMemoryJavaClasses() {
		HashMap<String, InMemoryJavaClassObject> returnedInMemoryJavaClasses = new HashMap<>();
		
		returnedInMemoryJavaClasses.putAll(inMemoryJavaClasses);
		
		return returnedInMemoryJavaClasses;
	}

	public void shallowClearInMemoryJavaFiles() {
		inMemoryJavaFiles.clear();
	}
	
	public void shallowClearInMemoryJavaClasses() {
		inMemoryJavaClasses.clear();
	}
	
	public void deepClearInMemoryJavaFiles() {
		inMemoryJavaFiles = new HashMap<>();
	}
	
	public void deepClearInMemoryJavaClasses() {
		inMemoryJavaClasses = new HashMap<>();
	}
	
	void listInMemoryJavaFiles(List<JavaFileObject> inMemoryJavaFileObjects, String packageName, boolean recurse) {
		inMemoryJavaFiles.entrySet().stream().forEach(inMemoryJavaFileEntry -> {
			this.<InMemoryJavaFileObject>listInMemoryJavaObjectsInternal(inMemoryJavaFileObjects, inMemoryJavaFileEntry, packageName, recurse);
		});
	}
	
	void listInMemoryJavaClasses(List<JavaFileObject> inMemoryJavaClassObjects, String packageName, boolean recurse) {
		inMemoryJavaClasses.entrySet().stream().forEach(inMemoryJavaClassEntry -> {
			this.<InMemoryJavaClassObject>listInMemoryJavaObjectsInternal(inMemoryJavaClassObjects, inMemoryJavaClassEntry, packageName, recurse);
		});
	}
	
	private <T extends JavaFileObject> void listInMemoryJavaObjectsInternal(List<JavaFileObject> inMemoryJavaObjects, Map.Entry<String, T> inMemoryJavaObjectEntry, String packageName, boolean recurse) {
		String resolvedPackageName = resolvePackageName(inMemoryJavaObjectEntry.getKey());
		
		if (recurse) {
			if (resolvedPackageName.contains(packageName))
				inMemoryJavaObjects.add(inMemoryJavaObjectEntry.getValue());
		} else {
			if (resolvedPackageName.equals(packageName))
				inMemoryJavaObjects.add(inMemoryJavaObjectEntry.getValue());
		}
	}
	
	private String resolvePackageName(String fullQualifiedName) {
		return fullQualifiedName.substring(0, fullQualifiedName.lastIndexOf('.'));
	}
}

