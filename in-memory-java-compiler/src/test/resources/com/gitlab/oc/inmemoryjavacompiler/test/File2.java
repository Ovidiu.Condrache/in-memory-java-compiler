package com.gitlab.oc.inmemoryjavacompiler.test;

public class File2 {
	
	private final String string;
	
	public File2(String string) {
		this.string = string;
	}
	
	public String getString() {
		return string;
	}
}
