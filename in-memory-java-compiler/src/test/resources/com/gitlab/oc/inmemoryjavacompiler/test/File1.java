package com.gitlab.oc.inmemoryjavacompiler.test;

import java.util.ArrayList;

public class File1 {
	
	private final ArrayList<Integer> arrayList;
	
	public File1() {
		arrayList = new ArrayList<>();
	}
	
	public ArrayList<Integer> getArrayList() {
		return arrayList;
	}
}
