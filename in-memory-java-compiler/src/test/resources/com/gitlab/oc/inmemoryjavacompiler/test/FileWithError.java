package com.gitlab.oc.inmemoryjavacompiler.test;

public class FileWithError {
	
	private final String string;
	
	public FileWithError(String string) {
		this.string = string;
	}
	
	public String getString() {
		return string;
	}
	
	public void setString(String string) {
		this.string = string;
	}
}