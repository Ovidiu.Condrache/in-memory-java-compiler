package com.gitlab.oc.inmemoryjavacompiler.test;

public class FileWithNestedClasses {
	
	private final String string;
	
	public FileWithNestedClasses(String string) {
		this.string = string;
	}
	
	public String getString() {
		return string;
	}
	
	static class InnerStaticClass1 {
		
	}
	
	private static class InnerStaticClass2 {
		
	}
	
	public class InnerClass1 {
		public class InnerClass2 {
			public class InnerClass3 {
				public String getOuterClassString() {
					return string;
				}
			}
		}
	}
	
	private class InnerClass4 {
		
	}
	
	public enum Enumeration {
		
	}
	
	public interface Interface {
		
	}
}