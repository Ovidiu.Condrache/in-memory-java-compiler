package com.gitlab.oc.inmemoryjavacompiler.test;

public class File3DependsOnFile2 {
	
	private final File2 file2;
	
	public File3DependsOnFile2(File2 file2) {
		this.file2 = file2;
		this.file2.getString();
	}
	
	public File2 getFile2() {
		return file2;
	}
}