package com.gitlab.oc.inmemoryjavacompiler.core;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.Arrays;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public final class InMemoryJavaCompilerTests {
	
	private static InMemoryJavaFileObject inMemoryJavaFileObjectFile1;
	private static InMemoryJavaFileObject inMemoryJavaFileObjectFile2;
	private static InMemoryJavaFileObject inMemoryJavaFileObjectFileWithError;
	private static InMemoryJavaFileObject inMemoryJavaFileObjectFileWithNestedClasses;
	private static InMemoryJavaFileObject inMemoryJavaFileObjectFile3DependsOnFile2;
	
	@BeforeClass
	public static void setUp() {
		String sourceCodeFile1 = readJavaFileIntoMemory(new File("src/test/resources/com/gitlab/oc/inmemoryjavacompiler/test/File1.java"));
		String sourceCodeFile2 = readJavaFileIntoMemory(new File("src/test/resources/com/gitlab/oc/inmemoryjavacompiler/test/File2.java"));
		String sourceCodeFileWithError = readJavaFileIntoMemory(new File("src/test/resources/com/gitlab/oc/inmemoryjavacompiler/test/FileWithError.java"));
		String sourceCodeFileWithNestedClasses = readJavaFileIntoMemory(new File("src/test/resources/com/gitlab/oc/inmemoryjavacompiler/test/FileWithNestedClasses.java"));
		String sourceCodeFile3DependsOnFile2 = readJavaFileIntoMemory(new File("src/test/resources/com/gitlab/oc/inmemoryjavacompiler/test/File3DependsOnFile2.java"));
		inMemoryJavaFileObjectFile1 = new InMemoryJavaFileObject("com.gitlab.oc.inmemoryjavacompiler.test.File1", sourceCodeFile1);
		inMemoryJavaFileObjectFile2 = new InMemoryJavaFileObject("com.gitlab.oc.inmemoryjavacompiler.test.File2", sourceCodeFile2);
		inMemoryJavaFileObjectFileWithError = new InMemoryJavaFileObject("com.gitlab.oc.inmemoryjavacompiler.test.FileWithError", sourceCodeFileWithError);
		inMemoryJavaFileObjectFileWithNestedClasses = new InMemoryJavaFileObject("com.gitlab.oc.inmemoryjavacompiler.test.FileWithNestedClasses", sourceCodeFileWithNestedClasses);
		inMemoryJavaFileObjectFile3DependsOnFile2 = new InMemoryJavaFileObject("com.gitlab.oc.inmemoryjavacompiler.test.File3DependsOnFile2", sourceCodeFile3DependsOnFile2);
	}
	
	@Before
	public void prepareTestEnvironment() {
		InMemoryJavaFileSystem.getInstance().shallowClearInMemoryJavaFiles();
		InMemoryJavaFileSystem.getInstance().shallowClearInMemoryJavaClasses();
	}

	@Test
	public void testSingleUnitCompilationSucceeds() {	
		assertTrue(InMemoryJavaCompiler.compile(inMemoryJavaFileObjectFile1, false));
	}
	
	@Test
	public void testMultipleUnitsCompilationSucceeds() {
		assertTrue(InMemoryJavaCompiler.compile(Arrays.asList(inMemoryJavaFileObjectFile1, inMemoryJavaFileObjectFile2), false));
	}
	
	@Test
	public void testSingleUnitCompilationFails() {
		assertFalse(InMemoryJavaCompiler.compile(inMemoryJavaFileObjectFileWithError, false));
	}
	
	@Test
	public void testMultipleUnitsCompilationFails() {
		assertFalse(InMemoryJavaCompiler.compile(Arrays.asList(inMemoryJavaFileObjectFile1, inMemoryJavaFileObjectFileWithError), false));
	}
	
	@Test
	public void testNestedClassesCompilationSucceeds() {
		assertTrue(InMemoryJavaCompiler.compile(inMemoryJavaFileObjectFileWithNestedClasses, false));
	}
	
	@Test
	public void testCompilationWithDependenciesSucceeds() {
		assertTrue(InMemoryJavaCompiler.compile(Arrays.asList(inMemoryJavaFileObjectFile3DependsOnFile2, inMemoryJavaFileObjectFile2), false));
	}
	
	@Test
	public void testCompilationWithDependenciesFails() {
		assertFalse(InMemoryJavaCompiler.compile(inMemoryJavaFileObjectFile3DependsOnFile2, false));
	}
	
	@Test
	public void testCompilationWithClasspathDependenciesSucceeds() {
		assertTrue(InMemoryJavaCompiler.compile(inMemoryJavaFileObjectFile2, false));
		assertTrue(InMemoryJavaCompiler.compile(inMemoryJavaFileObjectFile3DependsOnFile2, false));
	}
	
	@Test
	public void testCompilationWithClasspathDependenciesFails() {
		assertFalse(InMemoryJavaCompiler.compile(inMemoryJavaFileObjectFile3DependsOnFile2, false));
		assertTrue(InMemoryJavaCompiler.compile(inMemoryJavaFileObjectFile2, false));
	}
	
	@Test
	public void testCompilationWithSourcepathDependenciesSucceeds() {
		InMemoryJavaFileSystem.getInstance().putInMemoryJavaFile(inMemoryJavaFileObjectFile2);
		
		assertTrue(InMemoryJavaCompiler.compile(inMemoryJavaFileObjectFile3DependsOnFile2, true));
	}
	
	@Test
	public void testCompilationMatchesBytecode() throws IOException {
		byte[] javaCompilerBytecode = Files.readAllBytes(new File("src/test/resources/com/gitlab/oc/inmemoryjavacompiler/test/File1.class").toPath());
		
		InMemoryJavaCompiler.compile(inMemoryJavaFileObjectFile1, false);
		byte[] inMemoryJavaCompilerBytecode = InMemoryJavaFileSystem.getInstance().getInMemoryJavaClass("com.gitlab.oc.inmemoryjavacompiler.test.File1").getBytecode();
		
		assertTrue(Arrays.equals(javaCompilerBytecode, inMemoryJavaCompilerBytecode));
	}
	
	private static String readJavaFileIntoMemory(File javaFile) {
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(javaFile)));
			StringBuilder stringBuilder = new StringBuilder();
			String line;
			
			while ((line = bufferedReader.readLine()) != null)
				stringBuilder.append(line + '\n');
				
			bufferedReader.close();
			
			return stringBuilder.toString();
		} catch (Exception exception) {
			exception.printStackTrace(System.err);
			return null;
		}
	}
}
